function dydt = chemostat(t, y)

% y(1,:) = N
% y(2,:) = C

% Do this to preallocate
dydt = y;

alpha = 1;
kmax = 2;
kn = 1;
beta = 1;
gamma = 1;

dydt(1,:) = - alpha * y(1,:) + kmax * y(2,:) .* y(1,:) ./ (kn + y(2,:));
dydt(2,:) = - beta * kmax * y(2,:) .* y(1,:) / (kn + y(2,:)) - gamma * y(2,:);

end
