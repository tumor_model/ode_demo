% Coded this up following along with Nick 2/21/17.

tspan = [0, 10];
y0 = 1;

[t,y] = ode45(@F, tspan, y0);
% The @ tells ode45 that F is an externally defined function
% F = @(t,y) (-2 * y); % is the inline version, which does not need an @

plot(t, y)

% One dimensional ODE
[t2, y2] = ode45(@F2, tspan, y0);
plot(t2,y2)

% Two dinemnsional ODE, chemostat (growth of a bacterial culture)
% MIchaelis Menten

% Dimension of the ststem is number of rows in the Y matrix

y0 = [10, 10]; % [N, C]
[t3, y3] = ode45(@chemostat, tspan, y0);

plot(t3, y3)




