function dydt = F2(t, y)
    dydt = -t .* y;
end

